import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class CorrectRegistration {
    WebDriver driver;

    @BeforeMethod
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Iveto\\WebDrivers\\ChromeDriver\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://shop.pragmatic.bg/");


    }
//
//    @AfterMethod
//    public void turnDown() {
//        driver.quit();
//    }

    @Test
    public void register() {
        driver.findElement(By.cssSelector("li.dropdown span.hidden-xs")).click();
        driver.findElement(By.cssSelector("ul.dropdown-menu a[href='http://shop.pragmatic.bg/index.php?route=account/register']")).click();
        Assert.assertTrue(driver.getTitle().equalsIgnoreCase("Register Account"), "You are not in the correct page");

        driver.findElement(By.cssSelector("input[id='input-firstname']")).sendKeys("Ivelina");
        driver.findElement(By.cssSelector("input[id='input-lastname']")).sendKeys("Petrova");
        driver.findElement(By.cssSelector("input[id='input-email']")).sendKeys("green_nymph@abv.bg");
        driver.findElement(By.cssSelector("input[id='input-telephone']")).sendKeys("0988930080");
        driver.findElement(By.cssSelector("input[id='input-password']")).sendKeys("raphael9");
        driver.findElement(By.cssSelector("input[id='input-confirm']")).sendKeys("raphael9");


        WebElement radioButton = driver.findElement(By.cssSelector("label.radio-inline input[value='1']"));
        if (!radioButton.isSelected()) ;
        radioButton.click();
        Assert.assertTrue(radioButton.isSelected());
        WebElement agree = driver.findElement(By.cssSelector("input[name='agree']"));
        if (!agree.isSelected()) ;
        agree.isSelected();
        Assert.assertTrue(agree.isSelected());

        WebElement next = driver.findElement(By.cssSelector("input.btn-primary[value='Continue'] "));
        next.click();
        Assert.assertTrue(driver.getTitle().equalsIgnoreCase("Your Account Has Been Created!"), "You are not successfully register");

    }

}


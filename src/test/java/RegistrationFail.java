import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

import static org.testng.Assert.assertFalse;

public class RegistrationFail {
    WebDriver driver;

    @BeforeMethod
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Iveto\\WebDrivers\\ChromeDriver\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://shop.pragmatic.bg/");

    }
//
//    @AfterMethod
//    public void turnDown() {
//        driver.quit();
//    }

    @Test
    public void register() {
        driver.findElement(By.cssSelector("li.dropdown span.hidden-xs")).click();
        driver.findElement(By.cssSelector("ul.dropdown-menu a[href='http://shop.pragmatic.bg/index.php?route=account/register']")).click();
        Assert.assertTrue(driver.getTitle().equalsIgnoreCase("Register Account"), "You are not in the correct page");

        WebElement firstname = driver.findElement(By.cssSelector("input[id='input-firstname']"));
        Assert.assertTrue(firstname.getText().contains("First Name must be between 1 and 32 characters!"));

        WebElement lastname = driver.findElement(By.cssSelector("input[id='input-lastname']"));
        Assert.assertTrue(lastname.getText().contains("Last Name must be between 1 and 32 characters!"));

        WebElement email = driver.findElement(By.cssSelector("input[id='input-email']"));
        Assert.assertTrue(email.getText().contains("E-Mail Address does not appear to be valid!"));

        WebElement telephone = driver.findElement(By.cssSelector("input[id='input-telephone']"));
        Assert.assertTrue(telephone.getText().contains("Telephone must be between 3 and 32 characters!"));

        WebElement password = driver.findElement(By.cssSelector("input[id='input-password']"));
        Assert.assertTrue(password.getText().contains("Password must be between 4 and 20 characters!"));

        driver.findElement(By.cssSelector("input[id='input-confirm']"));


        WebElement agree = driver.findElement(By.cssSelector("input[name='agree']"));
        if (agree.isSelected()) ;
        agree.isSelected();
        assertFalse(agree.isSelected());

        WebElement next = driver.findElement(By.cssSelector("input.btn-primary[value='Continue'] "));
        next.click();
        WebElement alert = driver.findElement(By.cssSelector("div#account-register div.alert-dismissible"));
        Assert.assertTrue(alert.getText().contains("Warning: You must agree to the Privacy Policy!"));


    }
}



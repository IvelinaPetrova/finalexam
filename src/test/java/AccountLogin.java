import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class AccountLogin {
    WebDriver driver;

    @BeforeMethod
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Iveto\\WebDrivers\\ChromeDriver\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://shop.pragmatic.bg/");
    }

    @AfterMethod
    private void turnDown() {
        driver.quit();
    }

    @Test
    public void login() {
        driver.findElement(By.cssSelector("li.dropdown span.hidden-xs")).click();
        driver.findElement(By.linkText("Login")).click();

        driver.findElement(By.cssSelector("input#input-email")).sendKeys("green_nymph@abv.bg");
        driver.findElement(By.cssSelector("input#input-password")).sendKeys("raphael9");
        WebElement login = driver.findElement(By.cssSelector("input.btn-primary"));
        login.click();
        Assert.assertEquals(driver.getTitle(), "My Account");


    }
}
